const express = require('express');
const mongoose = require('mongoose');
const ShortUrl = require('./models/shortUrl')
const User = require('./models/users')
const bcrypt = require('bcrypt')
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const app = express()

require("./config/passport")(passport)


mongoose.connect('mongodb+srv://admin-elcan:MongoKer123@cluster0.qwtmb.mongodb.net/urlShortener?retryWrites=true&w=majority',{
    useNewUrlParser:true,useUnifiedTopology:true
})

app.use(express.static('public'))


//express session
app.use(session({
    secret : 'secret',
    resave : true,
    saveUninitialized : true
   }));
   app.use(passport.initialize());
   app.use(passport.session());
   app.use(flash());
   app.use((req,res,next)=> {
     res.locals.success_msg = req.flash('success_msg');
     res.locals.error_msg = req.flash('error_msg');
     res.locals.error  = req.flash('error');
   next();
   })

app.set('view engine','ejs')
app.use(express.urlencoded({extended:false}))

app.get('/',async (req,res)=>{
   
    if(!req.user){
        res.redirect('/welcome')
    }else {
        let shortUrls = await ShortUrl.find({id:req.user.id});
        res.render('index',{shortUrls: shortUrls,dirname:__dirname,user:req.user})
    }
    
})

app.get('/welcome',(req,res)=>{
    res.render('welcome',{user:req.user})
})


app.get('/register', (req,res)=>{
    res.render('register',{user:req.user});
})

//register post handle
app.post('/register',(req,res)=>{
    const {name,email, password, password2} = req.body;
    let errors = [];
    console.log(' Name ' + name+ ' email :' + email+ ' pass:' + password);
    if(!name || !email || !password || !password2) {
        errors.push({msg : "Please fill in all fields"})
    }
    //check if match
    if(password !== password2) {
        errors.push({msg : "passwords dont match"});
    }
    
    //check if password is more than 6 characters
    if(password.length < 6 ) {
        errors.push({msg : 'password atleast 6 characters'})
    }
    if(errors.length > 0 ) {
    res.render('register', {
        errors : errors,
        name : name,
        email : email,
        password : password,
        password2 : password2})
     } else {
        //validation passed
       User.findOne({email : email}).exec((err,user)=>{
        console.log(user);   
        if(user) {
            errors.push({msg: 'email already registered'});
            res.render('register',{errors,name,email,password,password2})  
           } else {
            const newUser = new User({
                name : name,
                email : email,
                password : password
            });
    
            //hash password
            bcrypt.genSalt(10,(err,salt)=> 
            bcrypt.hash(newUser.password,salt,
                (err,hash)=> {
                    if(err) throw err;
                        //save pass to hash
                        newUser.password = hash;
                    //save user
                    newUser.save()
                    .then((value)=>{
                        console.log(value)
                    req.flash('success_msg','You have now registered!')
                    res.redirect('/login');

                    })
                    .catch(value=> console.log(value));
                      
                }));
             }
       })
    }
    })


app.get('/login',(req,res)=>{
    res.render('login',{user:req.user});
})

app.get('/logout',(req,res)=>{
    req.logOut()
    res.redirect('/welcome')
})

app.post('/login',(req,res,next)=>{
    passport.authenticate('local',{
    successRedirect : '/',
    failureRedirect : '/login',
    failureFlash : true,
    })(req,res,next);
})

app.use((req,res,next)=>{
    res.locals.user = req.user;
    next()
})

app.post('/shortUrls', async (req,res)=>{
    console.log('SHORTENER PART');
    await ShortUrl.create({
        id : req.user.id,
        full: req.body.fullUrl,
    });
    res.redirect('/')
})

app.get('/:shortUrl',async (req,res)=>{
    const shortUrl = await ShortUrl.findOne({short:req.params.shortUrl})
    if(shortUrl == null) return res.sendStatus(404)
    shortUrl.clicks++
    shortUrl.save();
    res.redirect(shortUrl.full) 
})



app.listen(process.env.PORT || 3000,()=>{
    console.log('Server listening!');
})

